Gitlab issues to a static website.

You collaboratively write issues in gitlab's issue tracker, tag them,
upvote, and this script builds a little static website out of these
issues, sorted by upvotes, with a page per label, etc.

Now it's easy to add content (and upvote !) collaboratively.

Example: [https://factsplease.gitlab.io/LePen-facts/](https://factsplease.gitlab.io/LePen-facts/)

![site example](img.png)


## Install

Clone and

    make install

See [python-gitlab](https://github.com/gpocentek/python-gitlab) and
set your gitlab access key in the configuration file, i.e. put this in
`~/.python-gitlab.cfg` (or in `/etc`):

```
[global]
default = gitlab.com

[gitlab.com]
url = https://gitlab.com
private_token = private_token to get on your gitlab profile
http_username = username
http_password = ***
```

Add a `site.yml` config with one of the two settings:

```
pid: <int>  # project id
url: "gitlab.com/.../"  # project url
```

## Build

    make run && make serve
    # it runs on localhost:8080

no auto-reloading as for now.

requests are cached in `cache.db`, delete this file to get newer issues.


## Deploy with Gitlab pages

The goal is to host our site on Gitlab Pages. We will be able to
choose to re-build and publish it periodically, or when we press a
button. It is free, it is hosted on an url like
`http://groupname.gitlab.io/projectname`. See
[the doc](https://docs.gitlab.com/ee/user/project/pages/).

You need to create an account on Gitlab and create a new project. In
this project you can start creating issues and write your content in
there, tag them, etc. To build the website you need to create the
special `.gitlab-ci.yml` site.

_note: we are near to provide project skeletons._

You can check that your site built in the "Pipelines" menu or in
"Settings/Pages" where you should see your site's url.


Example `.gitlab-ci.yml`:

```
pages:
  stage: deploy
  image: python:3.5

  script:
    # gitlab config: a private token for authentication, necessary to login and get issues.
    - 'echo -e "private_token = $PRIVATE_TOKEN" >> ~/.python-gitlab.cfg'
    - git clone http://gitlab.com/vindarel/staticket && cd staticket

    # gitlab config: project url to get issues from (this project).
    - 'echo -e "[global]\ndefault = gitlab.com \n[gitlab.com]\nurl = https://gitlab.com \n" > ~/.python-gitlab.cfg'
    - 'echo "url: $CI_PROJECT_URL" > site.yml'
    - make install
    - make run
    - mv _build ../public
    - pwd && ls ../public
  artifacts:
    # GitLab Pages will only consider files in a directory called public.
    paths:
      - public
  only:
    - master
```

To use this you need to register a secret variable in your project
Settings (in "ci-cd"):

- `PRIVATE_TOKEN`: a private token that you generate on
  Settings/Integrations (and then register in Settings/ci-cd "secret
  variables"). We use it to replace the combo username/password to
  authenticate on Gitlab, to get the list of issues of the
  project. Gitlab's api requires an authentication.

The variable `CI_PROJECT_URL` is built in.

![secret variables](doc/secret-variables.png)


#### Build the site periodically

This functionnality
[is broken](https://gitlab.com/gitlab-org/gitlab-ce/issues/2989) on
Gitlab CI but
[is scheduled for 9.2](https://gitlab.com/gitlab-org/gitlab-ce/issues/30882)
and meanwhile it's doable with a trigger and a cron/anacron job somewhere.


## Dev

Activate the virtual env: `make activate`.

We're trying out [Coconut](http://coconut-lang.org/), a functional
superset of Python 3 ! Don't you worry, all Python 3 code is valid
Coconut.

We particularly like its (anaphoric) short lambdas, constructed with
`->` and inside of which we access the first argument with `_`. For
example:

    issues = filter( -> _.due_date is not None, issues)
    groups = sorted(groups.items(), key= -> _[0], reverse=True)  # group by year


instead of

    issues = filter(lambda(it): it.due_date is not None, issues)


_Note_: why not Github ? Gitlab is open-source and has more CI
features that we'll leverage. We must authenticate before querying the
api though. And we won't reject a contribution for Github support.

We use [Jade/Pug](https://pugjs.org/) templates instead of html (but
we must use macros in html, they are not supported in jade).

We use Bootstrap but you can show us something else :)

### Beware the cache

Gitlab requests are cached in `cache.db` (with `requests_cache`). To
bypass it you have to delete the file.

*todo: ease this process: settings, cli args, etc*


### Managing labels' colors

We don't want to write a white text on a bright label background. We
check the label background color and change the text to black if
needed. The `make run` warns us:

```
This color is too bright: #FFECDB
This color is too bright: #FFECDB
```

## Ideas / TODO list

- add usual pages of content (use the readme,…)
- a page to sort articles (issues) chronologically, for those with a
  due date
- filter meta issues (issues that speak about problems on the website,
  not content). With a "meta" tag, by "[META]" in their title.
- show comments
- …


dev:

- dev server auto-reload (watchdog)
- leverage a real static site builder ? (Nikola has many plugins, Lektor ?)
