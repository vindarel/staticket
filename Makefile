
install:
	pip install -r requirements.txt

activate:
	source bin/activate

ipython:
	coconut --jupyter console

run:
	coconut --run --strict staticket/utils.coco
	coconut --run --strict staticket/staticket.coco

serve:
	# needs inotify and co. When to leverage Lektor ?
	cd _build && python -m http.server 8080

test:
	pytest staticket/
